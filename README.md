
Copyright 2021 Alberto Daguerre Torres
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

___
# EL JUEGO DE LA VIDA #
El proposito es programar el famoso juego de la vida en java mediante los conocimientos adquiridos durante el curso.

___
### ¿En que consiste el juego de la vida? ###
Consiste en una malla cuadriculada, donde cada celda puede tomar uno de dos valores: "viva" o "muerta". Cada estado se puede representar con colores o con números, por ejemplo 1 = viva y 0 = muerta. Las reglas del Juego de la Vida son las siguientes. Para cada celda:

* Se cuentan cuántas células vivas hay en los ocho vecinos cercanos (cero si no hay vecinos, ocho si toda la vecindad está ocupada).
* Si la celda está viva, permanece viva sólo si tiene dos o tres vecinos (si hay uno o ninguno, se muere; si hay cuatro o más, también muere).
* Si la celda está muerta y tiene exactamente tres vecinos, nace una célula nueva (si hay otro número de vecinos, permanece muerta).

![Alt Text](https://milinviernos.org/wp-content/uploads/2020/04/15conway-gif-jumbo-v3.gif)

---
## Comandos para terminal LINUX
* make limpiar: Sirve para eliminar los archivos.class que estén creados.
* make compilar: Sirve para compilar el código.
* make ejecutar: Sirve para ejecutar el programa.
* make jar: Para que se genere el codigo de javadoc.
* make javadoc: Para que se genere javadoc. 

___
### Comandos para resto de terminales.
* javac -cp bin src/dominio/Tablero.java src/principal/Principal.java: Sirve para compilar(hay que eejcutar este cógio antes de ejecutar el siguiente).
* java -cp principal.Principal: Sirve para ejecutar el código(hay que ejecutar el primer código para poder realizar este).
* jar cvfm JuegoDeLaVida.jar Manifest.txt -C bin: Sirve para guerdar el javadoc.
* java jar: para que se ejecute el programa.

___
## Autor
* **[Alberto Daguerre Torres](https://daguerre02@bitbucket.org/daguerre02/segunda-practica.git)**

___
## Copyright
Yo, Alberto Daguerre Torres creador de este repositorio, concedo el uso de este para cualquier persona que se quiera basar en este código o cualquiera que quiera retocarlo o simplemente trastear con el.

![Copyright.png](../segunda-practica/Copyright.png)