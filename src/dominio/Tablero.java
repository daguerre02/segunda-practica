/**
 Copyright 2021 Alberto Daguerre Torres
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

package dominio;

import java.io.*;
import java.util.Scanner;

/**
 * Esta clase contiene todo lo necesario pada poder crear una matriz. Tanto una
 * cualquiera como una creada aleatoriamete.
 * 
 * @author Alberto Daguerre
 * @version final 24/03/2021
 */

public class Tablero {
    private static int DIMENSION = 30;
    private int[][] estadoActual = new int[DIMENSION + 2][DIMENSION + 2];// matriz que representa el estado actual.
    private int[][] estadoSiguiente = new int[DIMENSION + 2][DIMENSION + 2]; // Matriz que representa el estado
                                                                             // siguiente.

    /********************************************************
     * Lee el estado inicial de un fichero llamado ‘matriz‘.
     *
     ********************************************************/
    /**
     * clase de tipo void en la cual se imprime el fichero donde se encuentra la
     * matriz. Tiene una complejidad cuadrática o(n^2).
     */

    public void leerEstadoActual() {

        try {
            File fichero = new File("matriz");
            Scanner sc = new Scanner(fichero);
            for (int i = 0; i < DIMENSION; i++) {
                String linea = sc.nextLine(); // Cogemos la siguiente línea del fichero
                for (int j = 0; j < DIMENSION; j++) {
                    estadoActual[i + 1][j + 1] = Integer.parseInt(String.valueOf(linea.charAt(j)));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /********************************************************
     * Genera un estado inicial aleatorio. Para cada celda genera un número
     * aleatorio en el intervalo [0, 1). Si el número es menor que 0,5, entonces la
     * celda está inicialmente viva. En caso contrario, está muerta.
     *******************************************************/

    /**
     * Clase de tipo void la cual se encarga de generar una matriz de 30x30
     * aleatoriamente compuesta de ceros y unos mediante el método de Montecarlo. Su
     * complejidad es cuadrática o(n^2).
     */
    public void generarEstadoActualPorMontecarlo() {

        for (int i = 0; i < DIMENSION; i++) {
            for (int j = 0; j < DIMENSION; j++) {
                estadoActual[i + 1][j + 1] = ((int) (Math.random() * 2));
            }
        }
    }

    /********************************************************
     * Transita al estado siguiente según las reglas del juego de la vida.
     ********************************************************/

    /**
     * Clase de tipo void en la cual se encuentran las diferentes condiciones para
     * que se modifiquen los ceros y los unos de la matriz generada en el
     * estadoActual. Su complejidad es cuadrática en los bucles for o(n^2) y
     * constante en los ifs o(1).
     */
    public void transitarAlEstadoSiguiente() {
        estadoSiguiente = new int[DIMENSION + 2][DIMENSION + 2];

        for (int i = 1; i < DIMENSION + 1; i++) {
            for (int j = 1; j < DIMENSION + 1; j++) {
                int contador = 0;

                // Condiciones para los unos
                if (estadoActual[i][j + 1] == 1) {
                    contador += 1;
                }
                if (estadoActual[i - 1][j + 1] == 1) {
                    contador += 1;
                }
                if (estadoActual[i][j - 1] == 1) {
                    contador += 1;
                }
                if (estadoActual[i - 1][j - 1] == 1) {
                    contador += 1;
                }
                if (estadoActual[i + 1][j - 1] == 1) {
                    contador += 1;
                }
                if (estadoActual[i - 1][j] == 1) {
                    contador += 1;
                }
                if (estadoActual[i + 1][j] == 1) {
                    contador += 1;
                }
                if (estadoActual[i + 1][j + 1] == 1) {
                    contador += 1;
                }
                if (contador == 3) {
                    estadoSiguiente[i][j] = 1;
                } else if (estadoActual[i][j] == 1 && contador == 2) {
                    estadoSiguiente[i][j] = 1;
                } else {
                    estadoSiguiente[i][j] = 0;
                }
            }
        }

        estadoActual = estadoSiguiente;

    }

    /*******************************************************
     * Devuelve, en modo texto, el estado actual.
     *
     * @return el estado actual.
     *******************************************************/

    /**
     * Clase de tipo String en la cual cambio los ceros por un hueco en blanco y los
     * unos por "x" para que se vea mejor visualmente. Tiene una complejidad lineal
     * o(n) y complejidad constante en los ifs (1).
     *
     * @return devuelve la cadena con los cambios realizados en los ceros y unos.
     */

    @Override
    public String toString() {
        StringBuilder cadena = new StringBuilder();
        for (int i = 1; i < DIMENSION; i++) {
            for (int j = 1; j < DIMENSION; j++) {
                if (estadoActual[i][j] == 0) {
                    cadena.append(" ");
                } else {
                    cadena.append("x");
                }
            }
            cadena.append("\n");
        }
        return cadena.toString();
    }

}
